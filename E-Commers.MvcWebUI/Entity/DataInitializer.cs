﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace E_Commers.MvcWebUI.Entity
{
    public class DataInitializer:DropCreateDatabaseIfModelChanges<DataContext>
    {
        protected override void Seed(DataContext Context){
            List<Category> Categories = new List<Category>()
            {
                new Category() { Name = "Kamera", Description ="Kamera ürünleri"},
                new Category() { Name = "Bilgisayar", Description ="Bilgisayar ürünleri"},
                new Category() { Name = "Televizyon", Description ="Televizyon ürünleri"},
                new Category() { Name = "Telefon", Description ="Telefon ürünleri"},
                new Category() { Name = "Beyaz Eşya", Description ="Beyaz Eşya ürünleri"}
            };
            
            foreach(Category cat in Categories)
            {
                Context.Categories.Add(cat);
            }
            Context.SaveChanges();

            List<Product> Products = new List<Product>()
            {
                //KAMERA
                new Product() { Name="CANON IXUS 185 DİJİTAL KOMPAKT FOTOĞRAF MAKİNESİ (SİYAH)",Description="Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir.",Price =598.99, Stock=140 , IsApproved=true,CategoryId=1,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/CANON/thumb/v2-91746_small.jpg"},
                new Product() { Name="CANON POWERSHOT G7 X MARK II 20.1 MP DİJİTAL KOMPAKT FOTOĞRAF MAKİNESİ",Description="Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır.",Price =4.299, Stock=250 , IsApproved=true,CategoryId=1,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/CANON/thumb/v2-85985-4_small.jpg"},
                new Product() { Name="NIKON D7200 18-140 VR LENS 24.2 MP DİJİTAL SLR FOTOĞRAF MAKİNESİ",Description="Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır.",Price =8.898, Stock=140 , IsApproved=true,CategoryId=1,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/NIKON/thumb/v2-83057_small.jpg"},
                new Product() { Name="CANON EOS 4000D 18-55 + 75-300 18 MP 3,0 LCD EKRN SLR DIJITAL FOTOĞRAF MAKİNESİ",Description="1960'larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur.",Price =2.998, Stock=140 , IsApproved=true,CategoryId=1,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/CANON/thumb/v2-66181-7_small.jpg"},
                new Product() { Name="NIKON D5300 18-105 VR KIT 24.2 MP 3 DİJİTAL FOTOĞRAF MAKİNESİ",Description="Yaygın inancın tersine, Lorem Ipsum rastgele sözcüklerden oluşmaz.",Price =5.399, Stock=140 , IsApproved=true,CategoryId=1,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/NIKON/thumb/v2-83057_small.jpg"},

                //BİLGİSAYAR
                new Product() { Name="HOMETECH ALFA 110A INTEL Z3735F 1.83GHZ-2GB-32GB-11.6'' -W10 NOTEBOOK",Description="Kökleri M.Ö. 45 tarihinden bu yana klasik Latin edebiyatına kadar uzanan 2000 yıllık bir geçmişi vardır. ",Price =598.99, Stock=140 , IsApproved=true,CategoryId=2,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/HOMETECH/thumb/v2-91883_small.jpg"},
                new Product() { Name="HP 15-RA013NT CELERON N3060 1.6GHZ-4GB RAM-500GB HDD-15.6-INT-W10 NOTEBOOK",Description="Virginia'daki Hampden-Sydney College'dan Latince profesörü Richard McClintock, bir Lorem Ipsum pasajında geçen ve anlaşılması en güç sözcüklerden biri olan 'consectetur' sözcüğünün klasik edebiyattaki örneklerini incelediğinde kesin bir kaynağa ulaşmıştır.",Price =2.199, Stock=250 , IsApproved=true,CategoryId=2,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/ASUS/thumb/v2-91630_small.jpg"},
                new Product() { Name="OMEN by HP AMD Ryzen™ 7 1800X 3.6GHZ 16GB 3TB+256GB 6GB NVIDIA GTX1060 WIN10",Description="Lorm Ipsum, Çiçero tarafından M.Ö. 45 tarihinde kaleme alınan de Finibus Bonorum et Malorum (İyi ve Kötünün Uç Sınırları) eserinin 1.10.32 ve 1.10.33 sayılı bölümlerinden gelmektedir.",Price =8.898, Stock=140 , IsApproved=true,CategoryId=2,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/HP/thumb/v2-91491_small.jpg"},
                new Product() { Name="DELL XPS 15 CORE İ7 8750H 2.2GHZ-16GB RAM-512 SSD-GTX1050Ti 4GB-15.6 W10 PRO",Description="Bu kitap, ahlak kuramı üzerine bir tezdir ve Rönesans döneminde çok popüler olmuştur. Lorem Ipsum pasajının ilk satırı olan Lorem ipsum dolor sit amet 1.10.32 sayılı bölümdeki bir satırdan gelmektedir.",Price =17.399, Stock=140 , IsApproved=true,CategoryId=2,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/DELL/thumb/v2-91488_small.JPG"},
                new Product() { Name="MACBOOK PRO TOUCH BAR CORE İ5 2.3GHZ-8GB-256GBSSD-RETINA 13-INT-SILVER",Description="1500'lerden beri kullanılmakta olan standard Lorem Ipsum metinleri ilgilenenler için yeniden üretilmiştir.",Price =15.599, Stock=140 , IsApproved=true,CategoryId=2,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/APPLE/thumb/v2-91476_small.JPG"},
                
                //TELEVİZYON
                new Product() { Name="SAMSUNG UE 49NU8000 49'' 123 CM 4K UHD SMART TV,DAHİLİ UYDU ALICI",Description="Çiçero tarafından yazılan 1.10.32 ve 1.10.33 bölümleri de 1914 H. Rackham çevirisinden alınan İngilizce sürümleri eşliğinde özgün biçiminden yeniden üretilmiştir.",Price =7.099, Stock=140 , IsApproved=true,CategoryId=3,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/SAMSUNG/thumb/v2-90995_small.jpg"},
                new Product() { Name="VESTEL 40UD8400 40'' 102 CM 4K UHD SMART TV,HD DAHİLİ UYDU ALICI",Description="Yinelenen bir sayfa içeriğinin okuyucunun dikkatini dağıttığı bilinen bir gerçektir.",Price =2.829, Stock=250 , IsApproved=true,CategoryId=3,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/VESTEL/thumb/v2-91295_small.jpg"},
                new Product() { Name="SONY 49XF7096 49'' 123 CM 4K UHD SMART TV,DAHİLİ UYDU ALICI",Description="Lorem Ipsum kullanmanın amacı, sürekli 'buraya metin gelecek, buraya metin gelecek' yazmaya kıyasla daha dengeli bir harf dağılımı sağlayarak okunurluğu artırmasıdır.",Price =8.898, Stock=140 , IsApproved=true,CategoryId=3,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/SONY/thumb/v2-91159_small.jpg"},
                new Product() { Name="LG 55UK6100 55(140 CM) 4K UHD webOS SMART TV,DAHİLİ UYDU ALICI",Description="Şu anda birçok masaüstü yayıncılık paketi ve web sayfa düzenleyicisi, varsayılan mıgır metinler olarak Lorem Ipsum kullanmaktadır. ",Price =5.999, Stock=140 , IsApproved=true,CategoryId=3,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/LG/thumb/v2-91288-7_small.jpg"},
                new Product() { Name="AXEN 40'' 102 CM FHD TV",Description="Ayrıca arama motorlarında 'lorem ipsum' anahtar sözcükleri ile arama yapıldığında henüz tasarım aşamasında olan çok sayıda site listelenir.",Price =1.439, Stock=140 , IsApproved=true,CategoryId=3,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/AXEN/thumb/v2-91011-2_small.JPG"},

                //TELEFON
                new Product() { Name="SAMSUNG GALAXY J4 PLUS 16 GB AKILLI TELEFON SİYAH",Description="Yıllar içinde, bazen kazara, bazen bilinçli olarak (örneğin mizah katılarak), çeşitli sürümleri geliştirilmiştir.",Price =1.649, Stock=140 , IsApproved=true,CategoryId=4,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/SAMSUNG/thumb/v2-91977_small.jpg"},
                new Product() { Name="ASUS ZENFONE MAX PRO 64 GB AKILLI TELEFON SİYAH",Description="Lorem Ipsum pasajlarının birçok çeşitlemesi vardır.",Price =2.499, Stock=250 , IsApproved=true,CategoryId=4,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/ASUS/thumb/v2-91966_small.jpg"},
                new Product() { Name="HUAWEI MATE 20 LITE 64 GB AKILLI TELEFON SİYAH",Description="Ancak bunların büyük bir çoğunluğu mizah katılarak veya rastgele sözcükler eklenerek değiştirilmişlerdir.",Price =3.499, Stock=140 , IsApproved=true,CategoryId=4,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/HUAWEI/thumb/v2-91961_small.jpg"},
                new Product() { Name="REEDER P13 32 GB AKILLI TELEFON SİYAH",Description="Eğer bir Lorem Ipsum pasajı kullanacaksanız, metin aralarına utandırıcı sözcükler gizlenmediğinden emin olmanız gerekir.",Price =1.699, Stock=140 , IsApproved=true,CategoryId=4,IsHome=true,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/REEDER/thumb/v2-91913_small.JPG"},
                new Product() { Name="NOKIA 5 PRO AKILLI TELEFON MAVİ",Description="İnternet'teki tüm Lorem Ipsum üreteçleri önceden belirlenmiş metin bloklarını yineler. Bu da, bu üreteci İnternet üzerindeki gerçek Lorem Ipsum üreteci yapar. ",Price =1.899, Stock=140 , IsApproved=true,CategoryId=4,Image="http://cdn.vatanbilgisayar.com/UPLOAD/PRODUCT/NOKIA/thumb/v2-91907-1_small.jpg"},

                //BEYAZ EŞYA
                new Product() { Name="S283720 Diamond No Frost Buzdolabı",Description="Bu üreteç, 200'den fazla Latince sözcük ve onlara ait cümle yapılarını içeren bir sözlük kullanır.",Price =1.649, Stock=140 , IsApproved=true,CategoryId=5 ,IsHome=true},
                new Product() { Name="2181 DY Çekmeceli Derin Dondurucu",Description="Bu nedenle, üretilen Lorem Ipsum metinleri yinelemelerden, mizahtan ve karakteristik olmayan sözcüklerden uzaktır.",Price =2.499, Stock=250 , IsApproved=true,CategoryId=5},
                new Product() { Name="12143 CMK Çamaşır Makinesi",Description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",Price =5.979, Stock=140 , IsApproved=true,CategoryId=5},
                new Product() { Name="WF Bulaşık Makinesi",Description="Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. ",Price =4.056, Stock=140 , IsApproved=true,CategoryId=5},
                new Product() { Name="YK Kurutmalı Çamaşır Makinesi",Description="Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",Price =3.559, Stock=140 , IsApproved=true,CategoryId=5}
            };
            foreach (Product item in Products)
            {
                Context.Products.Add(item);
            }
            Context.SaveChanges();
            base.Seed(Context);
        }
    }
}