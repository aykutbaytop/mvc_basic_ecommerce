﻿using E_Commers.MvcWebUI.Entity;
using E_Commers.MvcWebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Commers.MvcWebUI.Controllers
{
    public class HomeController : Controller
    {
        DataContext _context = new DataContext();
        // GET: Home
        public ActionResult Index()
        {
            List<ProductModel> Products = _context.Products
                .Where(i => i.IsHome && i.IsApproved)//Anasayfa da gözüksün ve ürün onaylı ise
                .Select(
                    i => new ProductModel()
                    {
                        Id = i.Id,
                        Name = i.Name.Length>55 ? i.Name.Substring(0,55)+"...":i.Name,
                        Description = i.Description.Length>70 ? i.Description.Substring(0,70)+"...":i.Description,//Açıklamayı 70 karakter de kes ve ... ekle
                        Price = i.Price,
                        Stock = i.Stock,
                        Image = i.Image ?? "http://www.starwoodyapimarketizmir.com/static/default.jpg", // ürün resmi yoksa 
                        CategoryId = i.CategoryId
                    }
                ).ToList();
            return View(Products);
        }
        public ActionResult Details(int id)
        {
            return View(_context.Products.Where(i=>i.Id == id).FirstOrDefault());
        }
        public ActionResult List(int? id)
        {
            var Products = _context.Products
                .Where(i => i.IsApproved)
                .Select(
                    i => new ProductModel()
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Description = i.Description.Length > 70 ? i.Description.Substring(0, 70) + "..." : i.Description,
                        Price = i.Price,
                        Stock = i.Stock,
                        Image = i.Image ?? "http://www.starwoodyapimarketizmir.com/static/default.jpg",
                        CategoryId = i.CategoryId
                    }
                ).AsQueryable();
            if (id != null)
                Products = Products.Where(i => i.CategoryId == id);// Kategori seçilmiş ise ilgili ürünleri getir.

            return View(Products);
        }
        public PartialViewResult GetCategories()
        {
            return PartialView(_context.Categories.ToList());
        }
    }
}